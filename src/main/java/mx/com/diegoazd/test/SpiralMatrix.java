package mx.com.diegoazd.test;

public class SpiralMatrix {

  public int[] getSpiral(int[][] matrix) {
    int L = 0;
    int T = 0;
    int R = matrix[0].length-1;
    int B = matrix.length-1;
    int lenght = matrix.length*matrix[0].length;
    int current=0;
    int[] result = new int[lenght];

    if(R==0) {
      for(int i=0; i <= B; i++) {
        result[current++]=matrix[i][0];
      }

      return result;
    }else if(B == 0) {
      for(int i=0; i <= R; i++) {
        result[current++]=matrix[0][i];
      }

      return result;
    }


    while(L <= R && T <= B) {
      for(int i=L; i <= R; i++) result[current++]=matrix[T][i];
      T++;

      for(int i=T; i <= B; i++) result[current++]=matrix[i][R];
      R--;

      for(int i=R; i >= L; i--) result[current++]=matrix[B][i];
      B--;

      for(int i=B; i >= T; i--) result[current++]=matrix[i][L];
      L++;
    }

    return result;

  }
}

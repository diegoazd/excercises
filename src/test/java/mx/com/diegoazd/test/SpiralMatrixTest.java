package mx.com.diegoazd.test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SpiralMatrixTest {

 int[][] matrix;
 SpiralMatrix spiral;

  @Before
  public void setUp() {
    spiral = new SpiralMatrix();
  }

  @Test
  public void shouldGetJustOneElementFromMatrix() {
    matrix = new int[1][1];
    matrix[0][0] = 2;
    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 1);
    assertTrue(result[0] == 2);
  }

  @Test
  public void shouldEvaluateMatrixWith4Elements() {
    matrix = new int[2][2];
    fillMatrix();

    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 4);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 4);
    assertTrue(result[3] == 3);
  }

  @Test
  public void shouldEvaluateMatrixWith6Elements() {
    matrix = new int[3][3];
    fillMatrix();

    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 9);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 3);
    assertTrue(result[3] == 6);
    assertTrue(result[4] == 9);
    assertTrue(result[5] == 8);
    assertTrue(result[6] == 7);
    assertTrue(result[7] == 4);
    assertTrue(result[8] == 5);
  }

  @Test
  public void shouldEvaluateMatrixWith1And4Elements() {
    matrix = new int[1][4];
    fillMatrix();


    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 4);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 3);
    assertTrue(result[3] == 4);
  }

  @Test
  public void shouldEvaluateMatrixWith4And1Elements() {
    matrix = new int[4][1];
    fillMatrix();


    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 4);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 3);
    assertTrue(result[3] == 4);
  }

  @Test
  public void shouldEvaluateMatrixWith2And4Elements() {
    matrix = new int[2][4];
    fillMatrix();

    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 8);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 3);
    assertTrue(result[3] == 4);
    assertTrue(result[4] == 8);
    assertTrue(result[5] == 7);
    assertTrue(result[6] == 6);
    assertTrue(result[7] == 5);
  }

  @Test
  public void shouldEvaluateMatrixWith4And2Elements() {
    matrix = new int[4][2];
    fillMatrix();

    int[] result = spiral.getSpiral(matrix);

    assertTrue(result.length == 8);
    assertTrue(result[0] == 1);
    assertTrue(result[1] == 2);
    assertTrue(result[2] == 4);
    assertTrue(result[3] == 6);
    assertTrue(result[4] == 8);
    assertTrue(result[5] == 7);
    assertTrue(result[6] == 5);
    assertTrue(result[7] == 3);
  }

  private void fillMatrix() {
    int c=0;
    for(int i=0; i < matrix.length; i++)
      for(int j=0; j < matrix[0].length; j++)
        matrix[i][j] = ++c;
  }
}
